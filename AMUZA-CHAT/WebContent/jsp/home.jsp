<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html data-ng-app="Amuza">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<title></title>
<link rel="stylesheet" type="text/css" href="../css/home.css">
</head>
<body>
	<table>
		<tr>
			<td>
				<fieldset
					style="background-color: #E6E6FA; width: 300px; height: 600px;">
					<img src="" border=3 height=143 width=295></img>
					<ul id="home">

						<li><a href="#/" title="Home" class="selected">Home</a></li>
						<li><a href="#/registration" title="Register">Register</a></li>
						<li><a href="#/login" title="Login">Login</a></li>
						<li><a href="#/about" title="About Us">About Us</a></li>
						<li><a href="#/contact-us" title="Contact Us">Contact Us</a></li>
						<li><a href="#/news" title="News">News</a></li>
					</ul>
				</fieldset>
			</td>
			<td>
				<fieldset style="width: 975px; height: 600px;">
					<div data-ng-view></div>
				</fieldset>
			</td>
		</tr>
	</table>
	<script src="../scripts/Angular.min.js"></script>
	<script src="../scripts/angular-route.min.js"></script>
	<script src="../scripts/route.js"></script>
	<script src="../scripts/login-controller.js"></script>
	<script src="../scripts/registration-controller.js"></script>
	<script src="../scripts/angular-cookies.js"></script>
</body>
</html>