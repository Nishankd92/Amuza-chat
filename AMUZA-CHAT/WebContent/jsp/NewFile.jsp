<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<style type="text/css">
body {
	background-color: #708090;
	color: #5a5656;
	font-family: 'Open Sans', Arial, Helvetica, sans-serif;
	font-size: 16px;
	line-height: 1.5em;
}

a {
	text-decoration: none;
	margin-bottom: 10px;
	margin-left: 5px;
}

h1 {
	font-size: 1em;
}

strong {
	font-weight: bold;
}

.uppercase {
	text-transform: uppercase;
}

/* ---------- Register ---------- */
#Registration {
	margin-left: 30%;
	margin-top: 5%;
	width: 300px;
	float: center;
}

form fieldset input[type="text"], input[type="password"] {
	border: none;
	text-align: left;
	padding-left: 10px;
	border-radius: 3px;
	-moz-border-radius: 3px;
	-webkit-border-radius: 3px;
	font-family: 'Open Sans', Arial, Helvetica, sans-serif;
	font-size: 14px;
	height: 50px;
	outline: none;
	-webkit-appearance: none;
	border-radius: 3px;
}

form fieldset input[type="submit"] {
	background-color: #9ACD32;
	border: none;
	border-radius: 3px;
	-moz-border-radius: 3px;
	-webkit-border-radius: 3px;
	color: #f4f4f4;
	cursor: pointer;
	font-family: 'Open Sans', Arial, Helvetica, sans-serif;
	height: 50px;
	text-transform: uppercase;
	width: 300px;
	-webkit-appearance: none;
}

form fieldset a {
	color: #5a5656;
	font-size: 10px;
}

form fieldset a:hover {
	text-decoration: underline;
}

.btn-round {
	background-color: #5a5656;
	border-radius: 50%;
	-moz-border-radius: 50%;
	-webkit-border-radius: 50%;
	color: #f4f4f4;
	display: block;
	font-size: 12px;
	height: 50px;
	line-height: 50px;
	margin: 30px 125px;
	text-align: center;
	text-transform: uppercase;
	width: 100%;
	
}

div.social-wrap button {
	padding-left: 45px;
	height: 35px;
	background: none;
	border: none;
	display: block;
	background-size: 35px 35px;
	background-position: left center;
	background-repeat: no-repeat;
	border-radius: 4px;
	color: white;
	font-family: "Merriweather Sans", sans-serif;
	font-size: 14px;
	width: 205px;
	border-bottom: 2px solid transparent;
	border-left: 1px solid transparent;
	border-right: 1px solid transparent;
	box-shadow: 0 4px 2px -2px gray;
	text-shadow: rgba(0, 0, 0, .5) -1px -1px 0;
}

button#facebook {
	background-image:
		url(http://www.aaastaffing.net/AAANet/images/facebook-button.jpg);
	border-color: #2d5073;
	background-color: #3b5998;
	border: none;
	text-align: center;
	padding-left: 10px;
	border-radius: 3px;
	-moz-border-radius: 3px;
	-webkit-border-radius: 3px;
	font-family: 'Open Sans', Arial, Helvetica, sans-serif;
	font-size: 14px;
	height: 45px;
	outline: none;
	-webkit-appearance: none;
	border-radius: 3px;
}
</style>
</head>
<body>
	<div id="Registration" style="float: center;">
		<fieldset
			style="background-color: #E6E6FA; width: 147%; text-align: center;">
			<h1>
				<strong>Sign Up</strong>
			</h1>
		</fieldset>

		<form action="javascript:void(0);" method="get">
			<fieldset style="background-color: white;">

				<div class="social-wrap a">
					<button id="facebook" style="width: 91%; margin-left: 20px">Sign
						Up with Facebook</button>

				</div>
				<p style="color: black; text-align: center;">Or sign up with
					your email address</p>
				<table align="center" cellpadding="10" cellspacing="10" border="0">
					<tr>
						<td><input style="border-style: ridge;" type="text" required
							value="First Name"
							onBlur="if(this.value=='')this.value='First Name'"
							onFocus="if(this.value=='First Name')this.value='' "></td>

						<td><input style="border-style: ridge;" type="text" required
							value="Last Name"
							onBlur="if(this.value=='')this.value='Last Name'"
							onFocus="if(this.value=='Last Name')this.value='' "></td>
					</tr>

					<tr>
						<td colspan="2"><input
							style="width: 96%; border-style: ridge;" type="text" required
							value="Email" onBlur="if(this.value=='')this.value='Email'"
							onFocus="if(this.value=='Email')this.value='' "></td>
					</tr>
					<tr>
						<td colspan="2"><input
							style="width: 96%; border-style: ridge;" type="text" required
							value="Confirm Email Address"
							onBlur="if(this.value=='')this.value='Confirm Email Address'"
							onFocus="if(this.value=='Confirm Email Address')this.value='' "></td>
					</tr>
					<tr>
						<td colspan="2"><input
							style="width: 96%; border-style: ridge;" type="text" required
							value="Password" onBlur="if(this.value=='')this.value='Password'"
							onFocus="if(this.value=='Password')this.value='' "></td>
					</tr>
					<tr>
						<td colspan="2"><input style="width: 100%;" type="submit"
							value="Sign Up"></td>
					<tr>
				</table>
				<p style="color: black; text-align: center;">
					Already have an account?<a style="font-size: 14px; color: black;"
						href="#">Sign in</a>
				</p>
				<p style="text-align: center;">
					<a href="#" style="font-size: 14px; color: black;">Close</a>
				</p>
			</fieldset>
		</form>
	</div>
</body>
</html>