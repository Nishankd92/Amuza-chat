package com.amuza.DAO;

import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.amuza.models.User;
import com.amuza.utils.DBManager;

public class ProfileDAO {

	public String insertProfile(User user) {
		PreparedStatement statement;
		String result = null;
		try {
			statement = DBManager.getConnection().prepareStatement(QuerryRepo.INSERT_USER);
			statement.setString(1, user.getEmail());
			statement.setString(2, user.getFirstName());
			statement.setString(3, user.getLastName());
			statement.setInt(4, user.getAge());
			statement.setString(3, user.getMobile());
			statement.executeUpdate();
			result = "Successfully added";
		} catch (SQLException e) {
			result = "Failed";
			e.printStackTrace();
		}

		return result;
	}

	public String deleteUser(User user) {
		PreparedStatement statement;
		String result = null;
		try {
			statement = DBManager.getConnection().prepareStatement(QuerryRepo.DELETE_USER);
			statement.setString(1, user.getEmail());
			statement.setString(2, user.getFirstName());
			statement.setString(3, user.getLastName());
			statement.setInt(4, user.getAge());
			statement.setString(3, user.getMobile());
			statement.executeUpdate();
			result = "Successfully Deleted";
		} catch (SQLException e) {
			result = "Failed";
			e.printStackTrace();
		}

		return result;
	}

	/*
	 * public String getUser(String email) { PreparedStatement statement; String
	 * result = null; try { statement =
	 * DBManager.getConnection().prepareStatement(QuerryRepo.);
	 * statement.setString(1, user.getEmail()); statement.setString(2,
	 * user.getPassword()); statement.executeUpdate(); result =
	 * "Successfully added"; } catch (SQLException e) { result = "Failed";
	 * e.printStackTrace(); }
	 * 
	 * return result; }
	 */
	/*
	 * public String getAllUser() { PreparedStatement statement; String result =
	 * null; try { statement =
	 * DBManager.getConnection().prepareStatement(QuerryRepo.INSERT_USER);
	 * statement.setString(1, user.getEmail()); statement.setString(2,
	 * user.getPassword()); statement.executeUpdate(); result =
	 * "Successfully added"; } catch (SQLException e) { result = "Failed";
	 * e.printStackTrace(); }
	 * 
	 * return result; }
	 */
	public String updateUser(User user) {
		PreparedStatement statement;
		String result = null;
		try {
			statement = DBManager.getConnection().prepareStatement(QuerryRepo.UPDATE_USER);
			statement.setString(1, user.getEmail());
			statement.setString(2, user.getFirstName());
			statement.setString(3, user.getLastName());
			statement.setInt(4, user.getAge());
			statement.setString(3, user.getMobile());
			statement.executeUpdate();
			result = "Successfully added";
		} catch (SQLException e) {
			result = "Failed";
			e.printStackTrace();
		}

		return result;
	}

	public static void main(String[] args) {
		PreparedStatement statement;
		String result = null;
		try {
			statement = DBManager.getConnection().prepareStatement(QuerryRepo.INSERT_USER);
			statement.setString(1, "abcs");
			statement.setString(2, "qwe");
			statement.executeUpdate();
			result = "Successfully added";
		} catch (SQLException e) {
			result = "Failed";
			e.printStackTrace();
		}
		System.out.println(result);
	}
}
