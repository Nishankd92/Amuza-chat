package com.amuza.DAO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import com.amuza.models.User;
import com.amuza.utils.DBManager;

public class UserDAO {
	
	public String insertUser(User user) {
		PreparedStatement statement;
		String result = null;
		try {
			statement = DBManager.getConnection().prepareStatement(QuerryRepo.INSERT_USER);
			statement.setString(1, user.getEmail());
			statement.setString(2, user.getPassword());
			statement.setString(3, user.getMobile());
			statement.executeUpdate();
			result = "Successfully added";
		} catch (SQLException e) {
			result = "Failed";
			e.printStackTrace();
		}

		return result;
	}
	public String deleteUser(String email) {
		PreparedStatement statement;
		String result = null;
		try {
			statement = DBManager.getConnection().prepareStatement(QuerryRepo.DELETE_USER);
			statement.setString(1, email);
			statement.executeUpdate();
			result = "Successfully Deleted";
		} catch (SQLException e) {
			result = "Failed";
			e.printStackTrace();
		}

		return result;
	}
	public User getUser(String userEmail) {
		User user = new User();
		try {
			PreparedStatement statement = DBManager.getConnection().prepareStatement(QuerryRepo.GET_USER);
			statement.setString(1, userEmail);
			ResultSet rs = statement.executeQuery();
		    while(rs.next()){
		         user.setEmail(rs.getString("email"));
		         user.setPassword(rs.getString("password"));
		         user.setMobile(rs.getString("mobile"));
		         user.setFirstName(rs.getString("firstName"));
		         user.setLastName(rs.getString("lastName"));
		         user.setAge(rs.getInt("age"));
		         user.setGender(rs.getString("gender"));
		         user.setDeleted(rs.getBoolean("IsDeleted"));
		      }
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return user;
	}
	/*public String getAllUser() {
		PreparedStatement statement;
		String result = null;
		try {
			statement = DBManager.getConnection().prepareStatement(QuerryRepo.INSERT_USER);
			statement.setString(1, user.getEmail());
			statement.setString(2, user.getPassword());
			statement.executeUpdate();
			result = "Successfully added";
		} catch (SQLException e) {
			result = "Failed";
			e.printStackTrace();
		}

		return result;
	}*/
	public String updateUser(User user) {
		PreparedStatement statement;
		String result = null;
		try {
			statement = DBManager.getConnection().prepareStatement(QuerryRepo.UPDATE_USER);
			statement.setString(1, user.getEmail());
			statement.setString(2, user.getPassword());
			statement.executeUpdate();
			result = "Successfully added";
		} catch (SQLException e) {
			result = "Failed";
			e.printStackTrace();
		}

		return result;
	}

	public static void main(String[] args) {
		/*PreparedStatement statement;
		String result = null;
		try {
			statement = DBManager.getConnection().prepareStatement(QuerryRepo.INSERT_USER);
			statement.setString(1, "abcs");
			statement.setString(2, "qwe");
			statement.executeUpdate();
			result = "Successfully added";
		} catch (SQLException e) {
			result = "Failed";
			e.printStackTrace();
		}
		System.out.println(result);*/
		User user = new User();
		PreparedStatement statement = null;
		try {
			statement = DBManager.getConnection().prepareStatement(QuerryRepo.GET_USER);
			statement.setString(1, "%" + user.getEmail() + "%" );
			ResultSet rs = statement.executeQuery();
		    while(rs.next()){
		         String first = rs.getString("email");
		         String last = rs.getString("password");
		         int last1 = rs.getInt("user_id");

		         //Display values
		         System.out.print(", First: " + first);
		         System.out.println(", Last: " + last);
		         System.out.println(last1);
		      }
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}
	
}