package com.amuza.controllers;

import java.io.IOException;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.amuza.DAO.UserDAO;
import com.amuza.models.User;

@WebServlet("/register")
public class RegistrationController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private String result = null;

	public void doPost(HttpServletRequest request, HttpServletResponse response) {
		String email = request.getParameter("email");
		String mobile = request.getParameter("mobile");
		String password = request.getParameter("password");

		if (((email == null) || (email == "")) || ((mobile == null) || (mobile == ""))
				|| ((password == null) || (password == ""))) {

			result = "Invalid Data!!";

			try {
				response.getWriter().print(result);
				response.getWriter().flush();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		User user = new User();
		UserDAO dao = new UserDAO();
		user.setEmail(email);
		user.setPassword(password);
		user.setMobile(mobile);

		try {
			result = dao.insertUser(user);
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			response.getWriter().print(result);
			response.getWriter().flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
