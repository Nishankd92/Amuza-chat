package com.amuza.utils;

public class DBConstants {
	public static final String DB_USERNAME = "root";
	public static final String DB_PASSWORD = "root";
	public static final String SCHEMA = "amuza-chat";
	public static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
	public static final String DB_URL = "jdbc:mysql://localhost/" + SCHEMA;
}
