app.controller("registerController",
		[ '$scope', '$http', function($scope, $http) {
 
    $http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded; charset=utf-8";
     
    $scope.registerUser = function() {
    	var email = $scope.email;
    	var password = $scope.password;
    	var password2 = $scope.password2;
    	var mobile = $scope.mobile;
    	if( password === password2){
    		var data = {
                    'email' : email,
                    'password' : password,
                    'mobile' : mobile
                };
            $http({
                url : 'register',
                method : 'POST',
                params :data
            }).then(function(response) {
                console.log(response.data);
                $scope.message = response.data;
            }, function(response) {
                //fail case
                console.log(response);
                $scope.message = response;
            });
    	}
    	else{
    		$scope.error = "Password not matched!";
    		$scope.btn = "Refresh";
    	}
 
    };
} ]);