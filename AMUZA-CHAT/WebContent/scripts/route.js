var app = angular.module("Amuza", [ 'ngCookies', 'ngRoute']);
app.config(function($routeProvider, $cookiesProvider) {
	$routeProvider.when("/registration", {
		templateUrl : "../jsp/registration.jsp",
	}).when("/login", {
		templateUrl : "../jsp/loginPage.jsp"
	}).when("/forget", {
		templateUrl : "../jsp/forgotPassword.jsp"
	}).when("/profile", {
		templateUrl : "../jsp/profile.jsp"
	}).when("/groups", {
		templateUrl : "../jsp/groups.jsp"
	}).when("/settings", {
		templateUrl : "../jsp/settings.jsp"
	}).when("/news", {
		templateUrl : "../jsp/news.jsp"
	});
});