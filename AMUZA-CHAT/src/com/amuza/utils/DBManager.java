package com.amuza.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBManager {
	private static Connection conn = null;

	public static Connection getConnection() {

		try {
			// STEP 1: Register JDBC driver
			Class.forName(DBConstants.JDBC_DRIVER);
			// STEP 2: Open a connection
			System.out.println("Connecting to database...");
			conn = DriverManager.getConnection(DBConstants.DB_URL, DBConstants.DB_USERNAME, DBConstants.DB_PASSWORD);
		} catch (SQLException | ClassNotFoundException e) {
			System.out.println("Failed to get a connection...");
			e.printStackTrace();
		}
		return conn;
	}

}
