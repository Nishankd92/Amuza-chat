package com.amuza.controllers;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/abc")
public class changePasswordController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public void doPost(HttpServletRequest request, HttpServletResponse response) {
		String newPassword = request.getParameter("user.newPassword");
		String confirmPassword = request.getParameter("user.confirmPassword");
		System.out.println(newPassword + confirmPassword);
	}
}
