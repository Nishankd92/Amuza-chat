<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html data-ng-app="Amuza">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title></title>
<link rel="stylesheet" type="text/css" href="../css/home.css">
</head>
<body>
	<table>
		<tr>
			<td>
				<fieldset
					style="background-color: #E6E6FA; width: 300px; height: 600px;">
					<img
						src=""
						border=3 height=143 width=295></img>
					<ul id="userHome">

						<li><a href="#/home" title="Home">Home</a></li>
						<li><a href="#/profile" title="Profile">Profile</a></li>
						<li><a href="#/messages" title="Chats">Chats</a></li>
						<li><a href="#/news" title="news">News</a></li>
						<li><a href="#/settings" title="Settings">Settings</a></li>
						<li><a href="#/logout" title="logout">Logout</a></li>
					</ul>
				</fieldset>
			</td>
			<td>
				<fieldset  style="width: 640px;background-color:white; height: 600px;">
					<div data-ng-view></div>
				</fieldset>
			</td>
			<td><fieldset
					style="background-color: #E6E6FA; width: 300px; height: 600px;">
					<ul id="userHome">
						<li><a href="#" title="Contacts">Contacts</a></li>
					</ul>
				</fieldset></td>
		</tr>
	</table>
	<script src="../scripts/Angular.min.js"></script>
	<script src="../scripts/angular-route.min.js"></script>
	<script src="../scripts/route.js"></script>
	<script src="../scripts/angular-cookies.js"></script>
	<script src="../scripts/user-controller.js"></script>
</body>
</html>