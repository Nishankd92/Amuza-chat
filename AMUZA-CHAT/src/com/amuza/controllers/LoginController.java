package com.amuza.controllers;

import java.io.IOException;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.amuza.DAO.UserDAO;
import com.amuza.models.User;

@WebServlet("/login")
public class LoginController extends HttpServlet {

	private static final long serialVersionUID = 1L;
	private String result = null;

	public void doPost(HttpServletRequest request, HttpServletResponse response) {
		String email = request.getParameter("email");
		String password = request.getParameter("password");

		if (((email == null) || (email == "")) || ((password == null) || (password == ""))) {

			result = "Invalid Data!!";

			try {
				response.getWriter().print(result);
				response.getWriter().flush();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		UserDAO dao = new UserDAO();
		User user = dao.getUser(email);
		String message = null;
		if (user == null) {
			message = "User not found!";
		} else if (user.getPassword().equals(password)) {
			HttpSession session = request.getSession(true);
			session.setAttribute("email", user.getEmail());
			session.setAttribute("firstName", user.getFirstName());

		} else {
			message = "Password not matched!";
		}
		try {
			response.getWriter().print(message);
			response.getWriter().flush();

		} catch (IOException e) {
			e.printStackTrace();
		}

	}
}