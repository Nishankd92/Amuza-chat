app.controller('loginController', [ '$scope', '$http','$window',
		function ($scope, $http, $window) {
	$http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded; charset=utf-8";

			$scope.loginUser = function() {
				var data = {
					'email' : $scope.email,
					'password' : $scope.password
				};
				$http({
					url : 'login',
					method : 'POST',
					params : data
				}).then(function(response) {
					if(response.statusText === "OK"){
						$window.location.href = '/jsp/userHome.jsp';
					}
					else {
						$scope.message = response.data;
					}
					
					
	            }, function(response) {
	            	$scope.message = "Email not found!";	    
	            });
			};
		} ]);