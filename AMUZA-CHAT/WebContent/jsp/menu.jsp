<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Menu</title>
<style type="text/css">
body {
	background-color: #f4f4f4;
	color: #5a5656;
	font-family: 'Open Sans', Arial, Helvetica, sans-serif;
	font-size: 16px;
	line-height: 1.5em;
}

a {
	text-decoration: none;
}

h1 {
	font-size: 1em;
}

h1, p {
	margin-bottom: 10px;
}

strong {
	font-weight: bold;
}

.uppercase {
	text-transform: uppercase;
}

#menu, #menu ul {
	list-style: none;
	padding: 0;
	margin: 0;
}

#menu li {
	float: left;
	position: relative;
	line-height: 4.5em;
	width: 10em;
}

#menu li ul {
	position: absolute;
	margin-top: -1em;
	margin-left: .5em;
	display: none;
}

#menu ul li ul {
	margin-top: -3em;
	margin-left: 7em;
}

#menu a {
	display: block;
	border-right: 1px solid #fff;
	background: #E0F574;
	color: #3B3B3B;
	text-decoration: none;
	padding: 0 10px;
}

#menu a:hover {
	background-color: #5798B4;
	color: #fff;
}

#menu ul {
	border-top: 1px solid #fff;
}

#menu ul a {
	border-right: none;
	border-right: 1px solid #fff;
	border-bottom: 1px solid #fff;
	border-left: 1px solid #fff;
	background: #AEC245;
}
/* SHOW SUBMENU 1 */
#menu li:hover ul, #menu li.over ul {
	display: block;
	position: left;
}

#menu li:hover ul ul, #menu li.over ul ul {
	display: none;
}
/* SHOW SUBMENU 2 */
#menu ul li:hover ul, #menu ul li.over ul {
	display: block;
}
</style>
</head>
<body>
	<!-- begin the logo for the application -->
	<table width="100%">
		<tr valign="top" align="center">
			<td><img src="images/AmuzaLogo3.png" name="webLogo" width="425"
				height="50" border="0"></td>
		</tr>
	</table>
	<!-- end of logo -->
	<h2>
		<fmt:message key="loggedin.msg">
			<fmt:param value='${requestScope.userName}' />
		</fmt:message>
	</h2>
	<ul id="menu">
		<li><a href="#" title="Home" class="selected">Home</a></li>
		<li><a href="#" title="Register">Register</a></li>
		<li><a href="#" title="Login">Login</a></li>
		<li><a href="#" title="About Us">About Us</a></li>
		<li><a href="#" title="Contact Us">Contact Us</a>
			<ul>
				<li><a href="#" title="Email">Email</a></li>
				<li><a href="#" title="Mobile">Mobile</a></li>
			</ul></li>
	</ul>
	<!-- begin the footer for the application -->
	<div align="center">
		<hr SIZE="1" WIDTH="100%">
		<br /> Comments or Questions? <a>Email Amuza Customer Support</a><br />
		�2016 Amuza Chat<br />
	</div>
	<!-- end of footer -->
</body>
</html>